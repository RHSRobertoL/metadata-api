# README #

Metadata API using JSForce and Node to modify Listviews, picklist values, public groups, and sharing rules.
### What is this repository for? ###

* Metadata API using JSForce
* 1.0

### How do I get set up? ###

* Download Node
* Run "node install" in the folder where this project is located to download JSForce js library. 
* Update the index.js file to what organization you want to update.
* Run "node index.js" and it will process