var jsforce = require('jsforce');

var config = {
    username: 'rhs@rtr.com',
    password: 'Atrs9090',
    token: 'k4TONqjKZO1eAceYhMbaJgeY'
};
/*  username: 'rhs@farmadmin.com',
    password: 'Dogs0417',
    token: 'JaIQ7VtIvxQR79wfM7GCuit06'  */

var conn = new jsforce.Connection(config);

var query_community = "SELECT Id,Name from iar__iaCommunity__c WHERE iar__Parent_Company__c != ''"; 
var communityList = [];

var communityCustomFields = [
    'iar__Prospect__c.iar__My_Community__c', 
    'iar__Wait_List_Management__c.iar__My_Community__c'
];

// List View Mega Object
var defaultListViews = {
    iar__Prospect__c : {
        field: 'iar__My_Community__c',
        All: [
            {   fullName: 'iar__Prospect__c.All_Active',
                columns: [ 
                    'NAME',
                    'iar__Status__c',
                    'iar__Prospect_Status__c',
                    'iar__Inquirer_Email__c',
                    'iar__Inquirer_Phone_1__c',
                    'iar__Inquirer_Street_1__c',
                    'iar__Inquirer_Street_2__c',
                    'iar__Inquirer_State_Province__c',
                    'iar__Inquirer_ZIP_Postal_Code__c',
                    'iar__Prospect_Email__c',
                    'iar__Prospect_Phone_1__c',
                    'iar__Prospect_Street_1__c',
                    'iar__Prospect_State_Province__c',
                    'iar__Prospect_ZIP_Postal_Code__c',
                    'OWNER.FIRST_NAME' 
                ],
                filterScope: 'Everything',
                filters: { 
                    field: 'iar__Status__c', operation: 'equals', value: 'Active,Deposit,Wait List' 
                },
                label: 'All Active' 
            },
            {   fullName: 'iar__Prospect__c.All_Priority_List',
                columns: [ 
                    'NAME',
                    'iar__Status__c',
                    'iar__Prospect_Status__c',
                    'iar__Inquirer_Email__c',
                    'iar__Inquirer_Phone_1__c',
                    'iar__Inquirer_Street_1__c',
                    'iar__Inquirer_Street_2__c',
                    'iar__Inquirer_State_Province__c',
                    'iar__Inquirer_ZIP_Postal_Code__c',
                    'iar__Prospect_Email__c',
                    'iar__Prospect_Phone_1__c',
                    'iar__Prospect_Street_1__c',
                    'iar__Prospect_Street_2__c',
                    'iar__Prospect_State_Province__c',
                    'iar__Prospect_ZIP_Postal_Code__c' 
                ],
                filterScope: 'Everything',
                filters: { 
                    field: 'iar__Status__c', operation: 'equals', value: 'Wait List' 
                },
                label: 'All Priority List' 
            }
        ],
        Community: [
            
        ]
    },
    iar__Move_In_Planner__c : {
        field: 'iar__Community_ID__c',
        All: [
        ],
        Community: [
            {   fullName: 'iar__Move_In_Planner__c.Community_ALL',
                columns: 
                 [ 'iar__MIP_Date__c',
                   'iar__Refund_Date__c',
                   'NAME',
                   'iar__Resident_Last_Name__c',
                   'iar__Resident_First_Name__c',
                   'iar__Suite__c',
                   'iar__Expected_Admission_Date__c',
                   'iar__Create_Resident_s__c',
                   'iar__Move_In_Resident_s__c',
                   'iar__Second_Occupant_Type__c',
                   'iar__Sharing_First_Name__c',
                   'iar__Sharing_Last_Name__c',
                   'CREATED_DATE',
                   'iar__Entrance_Fee_Plan__c' ],
                filterScope: 'Everything',
                filters: 
                 { field: 'iar__Community_ID__c',
                   operation: 'equals',
                   value: '' },
                label: 'ALL',
                sharedTo: { group: '' } 
            },
            {   fullName: 'iar__Move_In_Planner__c.Community_Completed',
                columns: 
                 [ 'NAME',
                   'iar__Resident_Last_Name__c',
                   'iar__Resident_First_Name__c',
                   'iar__Suite__c',
                   'iar__Expected_Admission_Date__c',
                   'iar__Create_Resident_s__c',
                   'iar__Move_In_Resident_s__c',
                   'iar__Create_Lease__c',
                   'iar__Second_Occupant_Type__c',
                   'iar__Sharing_First_Name__c',
                   'iar__Sharing_Last_Name__c',
                   'CREATED_DATE',
                   'iar__Stay_Type__c',
                   'iar__MIP_Date__c' ],
                filterScope: 'Everything',
                filters: [ 
                    { field: 'iar__Move_In_Resident_s__c', operation: 'equals', value: '1' },
                    { field: 'iar__Community_ID__c', operation: 'equals', value: '' } ],
                label: 'Completed',
                sharedTo: { group: '' } 
            },
            {   fullName: 'iar__Move_In_Planner__c.Community_Current',
                columns: 
                 [ 'NAME',
                   'iar__Resident_Last_Name__c',
                   'iar__Resident_First_Name__c',
                   'iar__Suite__c',
                   'OWNER.FIRST_NAME',
                   'iar__MIP_Date__c',
                   'iar__Refund_Date__c',
                   'iar__Expected_Admission_Date__c',
                   'iar__Second_Occupant_Type__c',
                   'iar__Sharing_First_Name__c',
                   'iar__Sharing_Last_Name__c',
                   'CREATED_DATE',
                   'iar__Stay_Type__c' ],
                filterScope: 'Everything',
                filters: [ 
                    { field: 'iar__Move_In_Resident_s__c', operation: 'equals', value: '0' },
                    { field: 'iar__Community_ID__c', operation: 'equals', value: '' },
                    { field: 'iar__Refund_Date__c', operation: 'equals' } ],
                label: 'Current',
                sharedTo: { group: '' } 
            },
            {   fullName: 'iar__Move_In_Planner__c.Community_Today',
                columns: 
                 [ 'iar__Refund_Date__c',
                   'iar__Move_In_Resident_s__c',
                   'iar__Suite__c',
                   'NAME',
                   'iar__MIP_Date__c',
                   'iar__Create_Resident_s__c',
                   'iar__Move_In_Date__c',
                   'iar__Rent__c',
                   'iar__Rent_Type__c',
                   'iar__Resident_Last_Name__c',
                   'iar__Sharing_First_Name__c',
                   'iar__Services__c',
                   'iar__Sharing_Amount__c' ],
                filterScope: 'Everything',
                filters: 
                 { field: 'iar__Community_ID__c',
                   operation: 'contains',
                   value: '' },
                label: 'Today',
                sharedTo: { group: '' } 
            }
        ]
    },
    iar__Wait_List_Management : {
        field: 'iar__My_Community__c',
        All: [
        ],
        Community: [
        ]
    }
}

var defaultSharingRules = {
    'iar__Prospect__c' : {
        field : 'iar__CommunityFilter__c',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'iar__CommunityFilter__c',
                operation: 'contains',
                value: '' 
            }
        }
    },
    'iar__Suite__c' : {
        field : 'iar__Community_Filter__c',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'iar__Community_Filter__c',
                operation: 'contains',
                value: '' 
            }
        }
    }, 
    'iar__EMxReport__c' : {
        field : 'iar__Community_Filter__c',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'iar__Community_Filter__c',
                operation: 'contains',
                value: '' 
            }
        }
    },
    'iar__Resident_Transaction__c' : {
        field : 'iar__Community_Filter__c',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'iar__Community_Filter__c',
                operation: 'contains',
                value: '' 
            }
        }
    },
    'iar__Resident__c' : {
        field : 'iar__Community_Filter__c',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'iar__Community_Filter__c',
                operation: 'contains',
                value: '' 
            }
        }
    },
    'iar__iaCommunity__c' : {
        field : 'Name',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'Name',
                operation: 'contains',
                value: '' 
            }
        }
    },
    'iar__Move_In_Planner__c' : {
        field : 'iar__Community_Filter__c',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'iar__Community_Filter__c',
                operation: 'contains',
                value: '' 
            }
        }
    },
    'Account' : {
        field : 'iar__Community_Filter__c',
        Community : { 
            fullName: '',
            accessLevel: 'Edit',
            accountSettings: { 
                caseAccessLevel: 'None',
                contactAccessLevel: 'Edit',
                opportunityAccessLevel: 'None' 
            },
            label: '',
            sharedTo: { group: '' },
            criteriaItems: { 
                field: 'iar__Community_Filter__c',
                operation: 'contains',
                value: ''  
            }
        }
    }
}

var sharingRules = [
    'iar__Prospect__c',
    'iar__Suite__c', 
    'iar__EMxReport__c',
    'iar__Resident_Transaction__c',
    'iar__Resident__c',
    'iar__Recurring_Charges__c',
    'iar__iaCommunity__c',
    'iar__Move_In_Planner__c',
    'iar__Marketing_Events__c',
    'Account'
];

console.log('====> Start Metadata Converion for: ' + config.username);


conn.login(config.username, config.password + config.token, function(err, res) {    
    if (err) { return console.error(err); }
    
    /*
    // Get the Sharing Rules from an organization (To Copy them over somewhere else)
    conn.metadata.read('SharingRules', sharingRules, function(err,metadata){
        if (err) { console.error(err); }
        console.log('==> Sharing Rules');
        console.log('-------------Sharing Rules-----------------');
        console.log(metadata);
        for (var i = 0; i < metadata.length; i++) {
            //console.log('----');
            //console.log(metadata[i]);
            //console.log('== ' + metadata[i].fullName);
            //console.log(metadata[i].sharingCriteriaRules);
            //console.log('==');
        }
        console.log('-------------Sharing Rules-----------------');

        console.log('====> Finsihed Metadata Conversion for: ' + config.username)
    });
    */

    // Get Community List to create everything dynamically
    console.log('Generating community list::::');
    conn.query(query_community, function(err, res) {
        if (err) { return console.error(err); }

        for ( var i=0; i < res.records.length; i++) {
            communityList.push({Name:res.records[i].Name,Id:res.records[i].Id});
        }

        console.log('==> CustomField');
        // Community Picklist Options
        var picklistValues = [];
        for (var y=0; y <communityList.length; y++) {
           picklistValues.push({fullName: communityList[y].Name, default: 'false'});
        }
    
        for (var i=0; i < metadata.length; i++) {
            var meta = metadata[i];
            meta.picklist.picklistValues = picklistValues;
            console.log('   Upserted CustomField: ' + meta.fullName);
            conn.metadata.upsert('CustomField', meta, function(err, results) {
                if (err) { console.error(err); }
            });
        }
        console.log('   Community List: ' + communityList);

        console.log('==> CustomField');

        // Community Picklist Options Updates
        var picklistValues = [];
        for (var y=0; y <communityList.length; y++) {
           picklistValues.push({fullName: communityList[y].Name, default: 'false'});
        }
    
        for (var i=0; i < metadata.length; i++) {
            var meta = metadata[i];
            meta.picklist.picklistValues = picklistValues;
            console.log('   Upserted CustomField: ' + meta.fullName);
            conn.metadata.upsert('CustomField', meta, function(err, results) {
                if (err) { console.error(err); }
            });
        }
        
        // Create Public Group lists to upsert
        var groupList = [];
        for (var i = 0; i < communityList.length; i++) {
            groupList.push({
                doesIncludeBosses: true,
                fullName: communityList[i].Name.replace(/ /g,'_'), 
                name: communityList[i].Name
            });
        }

        // Upsert Public Groups (Usually public groups already created)
        console.log('Public Groups::::');
        conn.metadata.upsert('Group', groupList, function(err, results) {
            if (err) { console.error(err); }

            // Create List Views to Create
            var listViewsToUpsert = [];
            for (var objectListView in defaultListViews) {

                // Gets the List View Object 
                var theListViewList = defaultListViews[objectListView];

                // Add Default List Views to array.  (All array)
                [].push.apply(listViewsToUpsert,theListViewList.All);

                // loop through the Public Groups (Communities) and create Listviews dynamically
                for (var aGroup in groupList) {

                    // Get Public Group
                    var theGroup = groupList[aGroup];          

                    // Dynamically create List Views for Object for every community
                    for (var aListView in theListViewList.Community) {

                        // Copy the List View
                        var theListView = JSON.parse(JSON.stringify(theListViewList.Community[aListView]));

                        // Update the List View with unique fullname, label and add Public Group
                        theListView.fullName += '_' + theGroup.fullName;
                        theListView.label += ' - ' + theGroup.name;
                        theListView.sharedTo.group = theGroup.fullName;

                        // Update Community filter to correct community
                        if (Array.isArray(theListView.filters)) {
                            for (var aFilter in theListView.filters) {
                                try {
                                    if (theListView.filters[aFilter].field = theListViewList.field) {
                                        theListView.filters[aFilter].value = theGroup.name;
                                    }
                                } catch(err){}
                            }
                        } else {
                            try {
                                if (theListView.filters.field == theListViewList.field) {
                                    theListView.filters.value = theGroup.name;   
                                }
                            } catch(err){}
                        }

                        // Add the List View to the List
                        listViewsToUpsert.push(theListView);
                    }
                }
            }

            // Upsert the List Views
            console.log('ListView::::');
            console.log('   Number of List Views to create: ' + listViewsToUpsert.length);

            // NOTICE:::: MAX 10 LISTVIEWS AT A TIME!
            var chunk = 10;
            if (listViewsToUpsert.length.length < 10)
                chunk = listViewsToUpsert.length;
            for (var i=0; i < listViewsToUpsert.length; i+=chunk) {
                // Break array into chunks of 10 or less and upsert List Views
                conn.metadata.upsert('ListView', listViewsToUpsert.slice(i,i+chunk), function(err, results) {
                    if (err) { console.error(err); }
                    console.log(results);
                });
            }

            // Create List of Sharing Rules to Create
            var sharingRulesToUpsert = [];

            // Loop through Objects 
            for (var object in defaultSharingRules) {

                // Sharing Rule Object (needed to create)
                // object ::: Object API Name, sharingCriteriaRules ::: Sharing Rules
                var theSharingRuleJSON = {
                    fullName: object,
                    sharingCriteriaRules : []
                }
                // Sharing Rules for Object
                var sharingRule = defaultSharingRules[object];

                // loop through the Sharing Rules and create Sharing Rules dynamically for each community, update with community info, and public group
                for (var aGroup in groupList) {
                    
                    // The Public Group
                    var theGroup = groupList[aGroup];    

                    // The Copied Sharing Rule
                    var theSharingRule = JSON.parse(JSON.stringify(sharingRule.Community));

                    // Update sharing rule with community information
                    theSharingRule.fullName = theGroup.fullName;
                    theSharingRule.label = theGroup.name;
                    theSharingRule.sharedTo.group = theGroup.fullName;

                    // Update the Filter with community
                    try {
                        if (theSharingRule.criteriaItems.field ==sharingRule.field) {
                            theSharingRule.criteriaItems.value = theGroup.name;   
                        }
                    } catch(err){ console.log(err);}
                    
                    // Add Sharing rule to the Sharing rule object
                    theSharingRuleJSON.sharingCriteriaRules.push(theSharingRule);
                }
                // Add sharing rule object to array
                sharingRulesToUpsert.push(theSharingRuleJSON);
            }

            // Create Sharing Rules
            console.log('SharingRules::::');
            console.log('   Number of Sharing Rules to create: ' + sharingRulesToUpsert.length);
            conn.metadata.create('SharingRules', sharingRulesToUpsert, function(err,metadata){
                if (err) { console.error(err); }
                // If the sharing rules already exist it will not be created.
                console.log(metadata);
            });
        });        
    });
});