var jsforce = require('jsforce');
var fs = require('fs');

//Salesforce Credentials for target organization
var config = {
    username: 'rhs@CAHGAdmin.com',
    password: 'Dips0417',
    token: 'gUU9rek4cYp3o5EebAJcbr5F'
};

//Grabs local path
var path = process.cwd() + '/Data2.json';

//Open JSON File
console.log("Opening File: " + path);
fs.readFile(path, handleFile);

//Establishing connection
var conn = new jsforce.Connection({});


console.log("=========== Starting Pick List Inserting ==============\n");

//Defining variables
//var prospectStatus = ['iar__Prospect__c.iar__Prospect_Status__c'];
var prospectDepositType = ['iar__Prospect__c.iar__Deposit_Type__c','iar__Resident_Transactions__c.iar__Deposit_Type__c'];
var prospectPaymentType = ['iar__Prospect__c.iar__Payment_Type__c','iar__Resident_Transactions__c.iar__Payment_Type__c'];
var prospectWaitListType = ['iar__Prospect__c.iar__Wait_List_Type__c'];
var prospectAgeRange = ['iar__Prospect__c.iar__Age_Range__c','iar__Prospect__c.iar__Spouse_Age_Range__c']; //Age Ranges
var prospectAssessmentStatus = ['iar__Prospect__c.iar__Assessment_Status__c'];
var yesno = ['iar__Prospect__c.iar__Spouse_Is_a_Veteran__c','iar__Prospect__c.iar__Prospect_Is_a_Veteran__c'];
var createMoveInPlanner = ['iar__Prospect__c.iar__Create_Move_In_Planner__c'];
var annualIncomeRange = ['iar__Prospect__c.iar__Current_Annual_Income_Range__c','iar__Prospect__c.iar__Original_Annual_Income_Range__c'];
var homeValueRange = ['iar__Prospect__c.iar__Current_Home_Value_Range__c','iar__Prospect__c.iar__Current_Income_Producing_Assets_Range__c','iar__Prospect__c.iar__Original_Income_Producing_Asset_Range__c'];
var prospectDestination = ['iar__Prospect__c.iar__Destination__c'];
var prospectHouseOwnership = ['iar__Prospect__c.iar__Housing_Ownership__c'];
var prospectInqPhoneSource = ['iar__Prospect__c.iar__Inquirer_Phone_Source_1__c','iar__Prospect__c.iar__Inquirer_Phone_Source_2__c','iar__Prospect__c.iar__Inquirer_Phone_Source_3__c','iar__Prospect__c.iar__Original_Home_Value_Range__c'];
var prospectPrefix = ['iar__Prospect__c.iar__Inquirer_Prefix__c','iar__Prospect__c.iar__Prospect_Prefix__c','iar__Prospect__c.iar__Spouse_Prefix__c'];
var leadScoreEndReason = ['iar__Prospect__c.iar__Lead_Score_Alert_End_Reason_Code__c'];
var levelOfCare = ['iar__Prospect__c.iar__Level_of_Care_PL__c'];
var levelOfKnowledge = ['iar__Prospect__c.iar__Level_of_Knowledge__c'];
var livingSituation = ['iar__Prospect__c.iar__Living_Situation_PL__c'];
var prospectMailingLabel = ['iar__Prospect__c.iar__Mailing_Label__c'];
var maritalStatus = ['iar__Prospect__c.iar__Marital_Status__c'];
var prospectMindSet = ['iar__Prospect__c.iar__Mind_Set__c'];
var prospectOriginalIncome = ['iar__Prospect__c.iar__Original_Income_Range__c'];
var prospectOriginalNetWorth = ['iar__Prospect__c.iar__Original_Net_Worth__c'];
var prospectStayType = ['iar__Prospect__c.iar__Original_Stay_Type__c','iar__Prospect__c.iar__Stay_Type__c'];
var preferredMethodOfContact = ['iar__Prospect__c.iar__Preferred_Method_of_Contact__c'];
var prospectEducation = ['iar__Prospect__c.iar__Prospect_Education__c','iar__Prospect__c.iar__Spouse_Education__c'];
var gender = ['iar__Prospect__c.iar__Prospect_Gender__c','iar__Prospect__c.iar__Spouse_Gender__c'];
var prospectMilitaryBranch = ['iar__Prospect__c.iar__Prospect_Military_Branch__c','iar__Prospect__c.iar__Spouse_Military_Branch__c'];
var prospectOccupation = ['iar__Prospect__c.iar__Prospect_Occupation__c','iar__Prospect__c.iar__Spouse_Occupation__c'];
var prospectPhoneSource = ['iar__Prospect__c.iar__Prospect_Phone_Source_1__c','iar__Prospect__c.iar__Prospect_Phone_Source_2__c','iar__Prospect__c.iar__Prospect_Phone_Source_3__c','iar__Prospect__c.iar__Prospect_Phone_Source_4__c'];
var prospectPreferredMethodOfContact = ['iar__Prospect__c.iar__Prospect_Preferred_Method_of_Contact__c'];
var prospectPurchasingStyle = ['iar__Prospect__c.iar__Purchasing_Style__c'];
var prospectReasonCode = ['iar__Prospect__c.iar__Reason_Code__c'];
var prospectRecentCircumstance = ['iar__Prospect__c.iar__Recent_Circumstance__c'];
var prospectReferralType = ['iar__Prospect__c.iar__Referral_Type__c'];
var prospectRelationshipToProspect = ['iar__Prospect__c.iar__Relationship_to_Prospect__c'];
var prospectTimeFrame = ['iar__Prospect__c.iar__Timeframe__c'];
var prospectTypeOfInquiry = ['iar__Prospect__c.iar__Type_of_Inquiry__c'];
var prospectWebReasonForContacting = ['iar__Prospect__c.iar__Web_Reason_for_Contacting__c'];
var residentTransactionType = ['iar__Resident_Transactions__c.iar__Transaction_Type__c'];

var prospectPreferredFloorPlan = ['iar__Prospect__c.iar__Suite_Type__c'];
var prospectAlternateFloorPlan = ['iar__Prospect__c.iar__WL_Floor_Plan__c'];
var waitListType = ['iar__Wait_List_Management__c.iar__Child_Near_Community__c'];
var waitListPreferredFloorPlan = ['iar__Wait_List_Management__c.iar__Floor_Plan__c'];
var waitListAlternateFloorPlan = ['iar__Wait_List_Management__c.iar__Preferred_Floor_Plan__c'];
var suiteTypeList = [];
var communityCustomFields = [
    'iar__Prospect__c.iar__My_Community__c', 
    'iar__Wait_List_Management__c.iar__My_Community__c'
];
var Obj = null;

//Prospect Status Picklist Values
/*var prospectStatuspickListValues = ['Priority Depositor',
                      'Reservation Depositor',
                      'Cancelled Priority Depositor',
                      'Cancelled Reservation Depositor',
                      'Wants Priority',
                      'Urgent Need ASAP',
                      'Special Need within the next 1 to 3 months',
                      'Final Decision Process Within 30 Days',
                      'RH Decision Made - 1 to 3 Months',
                      'Basic Interest 4 - 6 Months',
                      'Waiting to Sell Home 6 - 12 Months',
                      'Preliminary Search Not Ready Yet',
                      'To Be Qualified within 10 Days',
                      'Unqualified Unknown',
                      'Do Not Contact',
                      'Lost Lead',
                      'Internal Use Only',
                      'Nurture Program',
                      '===== System Maintained Do Not Use =====',
                      'Resident',
                      'Temporary',
                      'Outreach',
                      'Duplicate'
                      ];
*/
//Deposit Type Picklist Values
//var depositTypepickListValues = ['10% Deposit','1000 Deposit'];

//Payment Type Picklist Values
//var paymentTypeValues = ['Check',
//                    'Cash',
//                    'No Deposit',
//                    'Paypal'];

//Wait List Type Picklist Values
//var waitListTypeValues = ['Priority','Wait List'];

//Suite Types Query
var suiteTypesQuery = 'SELECT Name FROM iar__Suite_Type__c';

//Community Query for Community Names
var query_community = "SELECT Id,Name from iar__iaCommunity__c WHERE iar__Parent_Company__c != ''";


//Grabs Json File 
function handleFile(err, data){
  if(err){ console.error(err); } 

    Obj= JSON.parse(data);
}

//Standard Login - Login into Salesfore with credentials
conn.login(config.username, config.password + config.token, function(err, res) {

	if (err) { return console.error(err); }
  else{ console.log("Log in successful!"); }
  

/*//Entering Picklist values for Prospect Status
conn.metadata.read('CustomField',prospectStatus, function(err, metadata) {
        if (err) { console.error(err); }
        else { console.log('\n========= Prospect Status =========\n'); }
      
        //console.log(JSON.stringify(metadata, null, '\t'));
        
        var pickListValues = [];

        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.prospectStatus.length; y++){
          console.log('- ' + Obj.prospectStatus[y]);
          pickListValues.push({fullName: Obj.prospectStatus[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          //Clearing values
          meta = null;
          pickListValues = null;
      });
*/

//Entering Picklist values for Prospect - Payment Type
conn.metadata.read('CustomField',prospectPaymentType, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Payment Type =========\n'); }
        
        var pickListValues = [];

        console.log('Values To Be Entered: ');

        console.log(JSON.stringify(metadata, null, '\t'))

        //Entering values and default status
        for (var y=0; y < Obj.paymentType.length; y++){
          console.log('- ' + Obj.paymentType[y]);
          pickListValues.push({fullName: Obj.paymentType[y], default: 'false'});
        }
          //Setting up picklist values
          for(var i=0; i < metadata.length; i++){
              var meta = metadata[i];
              meta.valueSet.valueSetDefinition.value = pickListValues;
              console.log('Updating ...' + meta.fullName);
              //Upserting values
              conn.metadata.upsert('CustomField',meta,function(err,results){
                if(err) { console.error(err); }
            }); 
        }

        if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
        
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Deposit Type
conn.metadata.read('CustomField',prospectDepositType, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Deposit Type =========\n'); }
        
        var pickListValues = [];

        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.depositType.length; y++){
          console.log('- ' + Obj.depositType[y]);
          pickListValues.push({fullName: Obj.depositType[y], default: 'false'});
        }
          //Setting up picklist values
          for(var i=0; i < metadata.length; i++){
              var meta = metadata[i];
              meta.valueSet.valueSetDefinition.value = pickListValues;
              console.log('Updating ...' + meta.fullName);
              //Upserting values
              conn.metadata.upsert('CustomField',meta,function(err,results){
                if(err) { console.error(err); }
            }); 
        }

        if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
        
          //Clearing values
          meta = null;
          pickListValues = null;
      });


//Entering Picklist values for Prospect - Wait List Type
conn.metadata.read('CustomField',prospectWaitListType, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Wait List Type =========\n'); }
        
        var pickListValues = [];

        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.waitListType.length; y++){
          console.log('- ' + Obj.waitListType[y]);
          pickListValues.push({fullName: Obj.waitListType[y], default: 'false'});
        }
          //Setting up picklist values
          console.log(JSON.stringify(metadata, null, '\t'))
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');
        
          //Clearing values
          meta = null;
          pickListValues = null;
      });


/*//Entering Picklist values for Wait List Management - Wait List Type
conn.metadata.read('CustomField',waitListType, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Wait List - Wait List Type =========\n'); }
        
        var pickListValues = [];

        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.waitListType.length; y++){
          console.log('- ' + Obj.waitListType[y]);
          pickListValues.push({fullName: Obj.waitListType[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          //conn.metadata.upsert('CustomField',meta,function(err,results){
            //if(err) { console.error(err);}
         //});
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });*/

//Entering Picklist values for Prospect - Age range
conn.metadata.read('CustomField',prospectAgeRange, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Age Range =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.ageRange.length; y++){
          console.log('- ' + Obj.ageRange[y]);
          pickListValues.push({fullName: Obj.ageRange[y], default: 'false'});
        }

        //Setting up picklist values
        for(var i=0; i < metadata.length; i++){
              var meta = metadata[i];
              meta.valueSet.valueSetDefinition.value = pickListValues;
              console.log('Updating ...' + meta.fullName);
              //Upserting values
              conn.metadata.upsert('CustomField',meta,function(err,results){
                if(err) { console.error(err); }
            }); 
        }

        if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');

        //Clearing values
        meta = null;
        pickListValues = null;
      });

//Entering Picklist values for Prospect - Assessment Status
conn.metadata.read('CustomField',prospectAssessmentStatus, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Assessment Status =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.assessmentStatus.length; y++){
          console.log('- ' + Obj.assessmentStatus[y]);
          pickListValues.push({fullName: Obj.assessmentStatus[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });


//Entering Picklist values for Prospect - Yes No Picklists
conn.metadata.read('CustomField',yesno, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Assessment Status =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.childNearFacility.length; y++){
          console.log('- ' + Obj.childNearFacility[y]);
          pickListValues.push({fullName: Obj.childNearFacility[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Create Move in planner
conn.metadata.read('CustomField',createMoveInPlanner, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Create Move in planner =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.createMoveInPlanner.length; y++){
          console.log('- ' + Obj.createMoveInPlanner[y]);
          pickListValues.push({fullName: Obj.createMoveInPlanner[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Annual Income
conn.metadata.read('CustomField',annualIncomeRange, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Annual income =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.annualIncome.length; y++){
          console.log('- ' + Obj.annualIncome[y]);
          pickListValues.push({fullName: Obj.annualIncome[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Home Value Range
conn.metadata.read('CustomField',homeValueRange, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Home Value Range =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.homeValueRange.length; y++){
          console.log('- ' + Obj.homeValueRange[y]);
          pickListValues.push({fullName: Obj.homeValueRange[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Destination
conn.metadata.read('CustomField',prospectDestination, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Destination =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.destination.length; y++){
          console.log('- ' + Obj.destination[y]);
          pickListValues.push({fullName: Obj.destination[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - House Ownership
conn.metadata.read('CustomField',prospectHouseOwnership, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - House Ownership =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.housingOwnership.length; y++){
          console.log('- ' + Obj.housingOwnership[y]);
          pickListValues.push({fullName: Obj.housingOwnership[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Inq Phone Source
conn.metadata.read('CustomField',prospectInqPhoneSource, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Inquirer Phone Source =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.phoneSource.length; y++){
          console.log('- ' + Obj.phoneSource[y]);
          pickListValues.push({fullName: Obj.phoneSource[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Prefix
conn.metadata.read('CustomField',prospectPrefix, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Prefix =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.inquirerPrefix.length; y++){
          console.log('- ' + Obj.inquirerPrefix[y]);
          pickListValues.push({fullName: Obj.inquirerPrefix[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });


//Entering Picklist values for Prospect - Lead Score End Reason
conn.metadata.read('CustomField',leadScoreEndReason, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Lead Score End Reason =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.leadScoreAlertReason.length; y++){
          console.log('- ' + Obj.leadScoreAlertReason[y]);
          pickListValues.push({fullName: Obj.leadScoreAlertReason[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Level Of Care
conn.metadata.read('CustomField',levelOfCare, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Level Of Care =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.levelOfCare.length; y++){
          console.log('- ' + Obj.levelOfCare[y]);
          pickListValues.push({fullName: Obj.levelOfCare[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Level Of Knowledge
conn.metadata.read('CustomField',levelOfKnowledge, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Level Of Knowledge =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.levelOfKnowledge.length; y++){
          console.log('- ' + Obj.levelOfKnowledge[y]);
          pickListValues.push({fullName: Obj.levelOfKnowledge[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Living Situation
conn.metadata.read('CustomField',livingSituation, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Living Situation =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.livingSituation.length; y++){
          console.log('- ' + Obj.livingSituation[y]);
          pickListValues.push({fullName: Obj.livingSituation[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Mailing Label
conn.metadata.read('CustomField',prospectMailingLabel, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Mailing Label =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.mailingLabel.length; y++){
          console.log('- ' + Obj.mailingLabel[y]);
          pickListValues.push({fullName: Obj.mailingLabel[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Marital Status
conn.metadata.read('CustomField',maritalStatus, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Living Situation =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.maritalStatus.length; y++){
          console.log('- ' + Obj.maritalStatus[y]);
          pickListValues.push({fullName: Obj.maritalStatus[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - MindSet
conn.metadata.read('CustomField',prospectMindSet, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - MindSet =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.mindSet.length; y++){
          console.log('- ' + Obj.mindSet[y]);
          pickListValues.push({fullName: Obj.mindSet[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Original Income Range
conn.metadata.read('CustomField',prospectOriginalIncome, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Original Income Range =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.originalIncomeRange.length; y++){
          console.log('- ' + Obj.originalIncomeRange[y]);
          pickListValues.push({fullName: Obj.originalIncomeRange[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Original Net Worth
conn.metadata.read('CustomField',prospectOriginalNetWorth, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Net Worth =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.originalNetWorth.length; y++){
          console.log('- ' + Obj.originalNetWorth[y]);
          pickListValues.push({fullName: Obj.originalNetWorth[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Stay Type
conn.metadata.read('CustomField',prospectStayType, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Stay Type =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.originalStayType.length; y++){
          console.log('- ' + Obj.originalStayType[y]);
          pickListValues.push({fullName: Obj.originalStayType[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Method of Contact
conn.metadata.read('CustomField',preferredMethodOfContact, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Method Of Contact =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.preferredMethodOfContact.length; y++){
          console.log('- ' + Obj.preferredMethodOfContact[y]);
          pickListValues.push({fullName: Obj.preferredMethodOfContact[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Education
conn.metadata.read('CustomField',prospectEducation, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Education =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.prospectEducation.length; y++){
          console.log('- ' + Obj.prospectEducation[y]);
          pickListValues.push({fullName: Obj.prospectEducation[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - gender
conn.metadata.read('CustomField',gender, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - gender =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.gender.length; y++){
          console.log('- ' + Obj.gender[y]);
          pickListValues.push({fullName: Obj.gender[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - prospectMilitaryBranch
conn.metadata.read('CustomField',prospectMilitaryBranch, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Military Branch =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.militaryBranch.length; y++){
          console.log('- ' + Obj.militaryBranch[y]);
          pickListValues.push({fullName: Obj.militaryBranch[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Occupation
conn.metadata.read('CustomField',prospectOccupation, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Occupation =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.prospectOccupation.length; y++){
          console.log('- ' + Obj.prospectOccupation[y]);
          pickListValues.push({fullName: Obj.prospectOccupation[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Phone Source
conn.metadata.read('CustomField',prospectPhoneSource, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Phone Source =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.prospectPhoneSource.length; y++){
          console.log('- ' + Obj.prospectPhoneSource[y]);
          pickListValues.push({fullName: Obj.prospectPhoneSource[y], default: 'false'});
        }

         for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 
            }

          if(metadata.length >0) console.log('\nSuccess in upserting ' + meta.fullName + '\n');
          
          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Preferred Method of Contact
conn.metadata.read('CustomField',preferredMethodOfContact, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Preferred Method of Contact =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.prospectPreferredMethodsOfContact.length; y++){
          console.log('- ' + Obj.prospectPreferredMethodsOfContact[y]);
          pickListValues.push({fullName: Obj.prospectPreferredMethodsOfContact[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });


//Entering Picklist values for Prospect - Prospect Purchashing style
conn.metadata.read('CustomField',prospectPurchasingStyle, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Preferred Method of Contact =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.purchasingStyle.length; y++){
          console.log('- ' + Obj.purchasingStyle[y]);
          pickListValues.push({fullName: Obj.purchasingStyle[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Reason Code
conn.metadata.read('CustomField',prospectReasonCode, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Reason Code =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.reasonCode.length; y++){
          console.log('- ' + Obj.reasonCode[y]);
          pickListValues.push({fullName: Obj.reasonCode[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Recent Circumstances
conn.metadata.read('CustomField',prospectRecentCircumstance, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Recent Circumstances =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.recentCircumstance.length; y++){
          console.log('- ' + Obj.recentCircumstance[y]);
          pickListValues.push({fullName: Obj.recentCircumstance[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Referral Type
conn.metadata.read('CustomField',prospectReferralType, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Referral Type =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.referralType.length; y++){
          console.log('- ' + Obj.referralType[y]);
          pickListValues.push({fullName: Obj.referralType[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Relationship To Prospect
conn.metadata.read('CustomField',prospectRelationshipToProspect, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Relationship To Prospect =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.relationshipToProspect.length; y++){
          console.log('- ' + Obj.relationshipToProspect[y]);
          pickListValues.push({fullName: Obj.relationshipToProspect[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Time Frame
conn.metadata.read('CustomField',prospectTimeFrame, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Time Frame =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.timeFrame.length; y++){
          console.log('- ' + Obj.timeFrame[y]);
          pickListValues.push({fullName: Obj.timeFrame[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - Type Of Inquiry
conn.metadata.read('CustomField',prospectTypeOfInquiry, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Type Of Inquiry =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.typeOfInquiry.length; y++){
          console.log('- ' + Obj.typeOfInquiry[y]);
          pickListValues.push({fullName: Obj.typeOfInquiry[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });


//Entering Picklist values for Prospect - Web Reason For Contacting
conn.metadata.read('CustomField',prospectWebReasonForContacting, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Web Reason For Contacting =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.webReasonForContacting.length; y++){
          console.log('- ' + Obj.webReasonForContacting[y]);
          pickListValues.push({fullName: Obj.webReasonForContacting[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });

//Entering Picklist values for Prospect - resident Transaction Type
conn.metadata.read('CustomField',residentTransactionType, function(err, metadata) {
        if (err) { console.error(err); }
        else{ console.log('\n========= Prospect - Web Reason For Contacting =========\n'); }        
        
        var pickListValues = [];
        console.log('Values To Be Entered: ');

        //Entering values and default status
        for (var y=0; y < Obj.transactionType.length; y++){
          console.log('- ' + Obj.transactionType[y]);
          pickListValues.push({fullName: Obj.transactionType[y], default: 'false'});
        }
          //Setting up picklist values
          var meta = metadata;
          meta.valueSet.valueSetDefinition.value = pickListValues;

          //Upserting values
          conn.metadata.upsert('CustomField',meta,function(err,results){
            if(err) { console.error(err);}
         });
          console.log('\nSuccess in upserting ' + meta.fullName + '\n');

          //Clearing values
          meta = null;
          pickListValues = null;
      });


//Querying Suite Types
conn.query(suiteTypesQuery,function(err,res){
    if(err){ console.error(err); }
    else{ console.log("\n========= Querying Suite Types ===========\n"); }

    console.log("Suite Types to be entered: ");

    for (var i=0; i < res.records.length; i++){
        console.log(res.records[i].Name);
        suiteTypeList.push({Name:res.records[i].Name,Id:res.records[i].Id});
    }

    var temp2 = [];

    for (var y=0; y <suiteTypeList.length; y++) {
           temp2.push({fullName: suiteTypeList[y].Name, default: 'false'});
        }

    //console.log("Testing Point " + JSON.stringify(temp2));

    //Entering  Prospect Alternate Floor Plans Picklist Values
    conn.metadata.read('CustomField', prospectAlternateFloorPlan, function(err, metadata){
        if(err){ console.error(err); }
        else { console.log("\n========= Prospect - Alternate Floor Plan =============\n"); }

        if(suiteTypeList.length > 0){
            var meta = metadata;
            meta.valueSet.valueSetDefinition.value = temp2;

            //Upserting values
             conn.metadata.upsert('CustomField',meta,function(err,results){
                if(err) { console.error(err);}
             });
            console.log('Success in upserting ' + meta.fullName + '\n');
            meta = null;
        }else{
          console.log("No Values Found");
        }

      });


    //Entering  Prospect Preferred Floor Plans Picklist Values
    conn.metadata.read('CustomField', prospectPreferredFloorPlan, function(err, metadata){
        if(err){ console.error(err); }
        else { console.log("\n========= Prospect - Preferred Floor Plan =============\n"); }

        console.log()

        if(suiteTypeList.length > 0){
            var meta = metadata;
            meta.valueSet.valueSetDefinition.value = temp2;

            //Upserting values
              conn.metadata.upsert('CustomField',meta,function(err,results){
                if(err) { console.error(err);}
             });
            console.log('Success in upserting ' + meta.fullName + '\n');
            meta = null;
        }else{
          console.log("No Values Found");
        }
    });

    //Entering  Wait List Management Preferred Floor Plans Picklist Values
    conn.metadata.read('CustomField', waitListPreferredFloorPlan, function(err, metadata){
        if(err){ console.error(err); }
        else { console.log("\n========= Wait List - Preferred Floor Plan =============\n"); }

        if(suiteTypeList.length > 0){
            var meta = metadata;
            meta.valueSet.valueSetDefinition.value = temp2;

            //Upserting values
              conn.metadata.upsert('CustomField',meta,function(err,results){
                if(err) { console.error(err);}
             });
            console.log('Success in upserting ' + meta.fullName + '\n');
            meta = null;
        }else{
          console.log("No Values Found");
        }
    });

    //Entering  Wait List Management Alternate Floor Plans Picklist Values
    conn.metadata.read('CustomField', waitListAlternateFloorPlan, function(err, metadata){
        if(err){ console.error(err); }
        else { console.log("\n========= Wait List - Alternate Floor Plan =============\n"); }

        if(suiteTypeList.length > 0){
            var meta = metadata;
            meta.valueSet.valueSetDefinition.value = temp2;

            //Upserting values
              conn.metadata.upsert('CustomField',meta,function(err,results){
                if(err) { console.error(err);}
             });
            console.log('Success in upserting ' + meta.fullName + '\n');
            meta = null;
        }else{
          console.log("No Values Found");
        }
    });

});

//Querying community names and updating my community picklists with them
conn.query(query_community, function(err, res) {
        if (err) { return console.error(err); }
        else { console.log('\n======= Generating community list ========\n'); }

        var pickListValues = [];
        
        console.log("Values to be entered:");

        // Community Picklist Options
        for (var y=0; y <res.records.length; y++) {
            console.log("- " + res.records[y].Name);
           pickListValues.push({fullName: res.records[y].Name, default: 'false'});
        }

        //Testing Result Object Output
        //console.log('Outputting Object: ' + JSON.stringify(pickListValues, null, '\t'));

        conn.metadata.read('CustomField',communityCustomFields, function(err, metadata){
          if(err){ console.error(err); }
          else{ console.log('\n====== Reading Community Fields Objects =========\n')}

          if(pickListValues.length > 0){
            //Testing Metadata Output
            //console.log('Outputting metadata ' + JSON.stringify(metadata, null, '\t') + '\n');
            
            //Setting up picklist values
            for(var i=0; i < metadata.length; i++){
                var meta = metadata[i];
                meta.valueSet.valueSetDefinition.value = pickListValues;
                console.log('Updating ...' + meta.fullName);
                //Upserting values
                conn.metadata.upsert('CustomField',meta,function(err,results){
                  if(err) { console.error(err); }
              }); 

              console.log('Process Finished');
            }
          }else{
            //No Community Names Found
            console.log('No Values Found');
          }
        });
    });
});

