// List View Mega Object
var object = {
    iar__Prospect__c : {
        field : 'iar__My_Community__c',
        All : [
            {   fullName: 'iar__Prospect__c.All_Active',
                columns: [ 
                    'NAME',cd 
                    'iar__Status__c',
                    'iar__Prospect_Status__c',
                    'iar__Inquirer_Email__c',
                    'iar__Inquirer_Phone_1__c',
                    'iar__Inquirer_Street_1__c',
                    'iar__Inquirer_Street_2__c',
                    'iar__Inquirer_State_Province__c',
                    'iar__Inquirer_ZIP_Postal_Code__c',
                    'iar__Prospect_Email__c',
                    'iar__Prospect_Phone_1__c',
                    'iar__Prospect_Street_1__c',
                    'iar__Prospect_State_Province__c',
                    'iar__Prospect_ZIP_Postal_Code__c',
                    'OWNER.FIRST_NAME' 
                ],
                filterScope: 'Everything',
                filters: { 
                    field: 'iar__Status__c', operation: 'equals', value: 'Active,Deposit,Wait List' 
                },
                label: 'All Active' 
            },
            {   fullName: 'iar__Prospect__c.All_Priority_List',
                columns: [ 
                    'NAME',
                    'iar__Status__c',
                    'iar__Prospect_Status__c',
                    'iar__Inquirer_Email__c',
                    'iar__Inquirer_Phone_1__c',
                    'iar__Inquirer_Street_1__c',
                    'iar__Inquirer_Street_2__c',
                    'iar__Inquirer_State_Province__c',
                    'iar__Inquirer_ZIP_Postal_Code__c',
                    'iar__Prospect_Email__c',
                    'iar__Prospect_Phone_1__c',
                    'iar__Prospect_Street_1__c',
                    'iar__Prospect_Street_2__c',
                    'iar__Prospect_State_Province__c',
                    'iar__Prospect_ZIP_Postal_Code__c' 
                ],
                filterScope: 'Everything',
                filters: { 
                    field: 'iar__Status__c', operation: 'equals', value: 'Wait List' 
                },
                label: 'All Priority List' 
            }
        ],
        Community : [
            {   fullName: 'iar__Prospect__c.My_Prospects',
                columns: [ 
                    'NAME',
                    'iar__Status__c',
                    'iar__Prospect_Status__c',
                    'iar__Inquirer_Email__c',
                    'iar__Inquirer_Phone_1__c',
                    'iar__Inquirer_Street_1__c',
                    'iar__Inquirer_Street_2__c',
                    'iar__Inquirer_State_Province__c',
                    'iar__Inquirer_ZIP_Postal_Code__c',
                    'iar__Prospect_Email__c',
                    'iar__Prospect_Phone_1__c',
                    'iar__Prospect_Street_1__c',
                    'iar__Prospect_Street_2__c',
                    'iar__Prospect_State_Province__c',
                    'iar__Prospect_ZIP_Postal_Code__c'
                ],
                filterScope: 'Mine',
                label: 'My Prospects' 
            },
            {   fullName: 'iar__Prospect__c.My_Priority_List',
                columns: 
                 [ 'NAME',
                   'iar__Status__c',
                   'iar__Prospect_Status__c',
                   'iar__Inquirer_Email__c',
                   'iar__Inquirer_Phone_1__c',
                   'iar__Inquirer_Street_1__c',
                   'iar__Inquirer_Street_2__c',
                   'iar__Inquirer_State_Province__c',
                   'iar__Inquirer_ZIP_Postal_Code__c',
                   'iar__Prospect_Email__c',
                   'iar__Prospect_Phone_1__c',
                   'iar__Prospect_Street_1__c',
                   'iar__Prospect_Street_2__c',
                   'iar__Prospect_State_Province__c',
                   'iar__Prospect_ZIP_Postal_Code__c' ],
                filterScope: 'Mine',
                filters: { 
                    field: 'iar__Status__c', operation: 'equals', value: 'Wait List' 
                }, 
                label: 'My Priority List' 
            }
        ]
    },
    iar__Wait_List_Management : {
        field : 'iar__My_Community__c',
        All : [
        ],
        Community : [
        ]
    }
}
